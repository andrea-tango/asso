import os, sys, shutil	
import numpy as np
import random

sys.path.insert(0, "SSO")

from SSO import SSO
from Benchmarks import Benchmarks


class SSO_ben(SSO):

	def __init__(self, n_salps=100, max_iterations=100, path=".", repetition=0, ID=None, seed=None, verbose=True):
		super().__init__(n_salps=n_salps, max_iterations=max_iterations, path=path, repetition=repetition, ID=ID, seed=seed, verbose=verbose)

		
	def update_fitness(self):

		benchmarks  = Benchmarks()

		try:
			for s in self.get_salps():
				fitness = eval("benchmarks." + self.get_function() + "(s.get_X())")
				s.set_fitness(fitness)
		except:
			print("* Error! The function", self.get_function(), "is not defined. Please, provide a valid function.")
			exit(-1)
		
		
		
if __name__ == '__main__':

	benchmarks  = Benchmarks()
	boundaries  = benchmarks.Boundaries()

	repetitions = 30
	dimensions  = 2

	n_salps = 50
	max_it  = 100

	functions  = ["Ackley_shifted",
				  "Alpine_shifted",
				  "Rosenbrock_shifted",
				  "Sphere_shifted"]

	for function in functions:
		print("*"*100)
		print(" * Optimizing function: %s"%function)

		path = "Results"+os.sep+"SSO"+os.sep+function+"_"+str(dimensions)

		bounds   = boundaries[function]
		l_bounds = [bounds[0]]
		u_bounds = [bounds[1]]
		
		for rep in range(repetitions):
			print(" \t Running optimization %3d/%d"%(rep+1,repetitions))
			sso = SSO_ben(n_salps=n_salps,
						  max_iterations=max_it,
						  path=path,
						  repetition=rep,
						  verbose=False,
						  seed=rep)

			sso.solve(function=function,
				      dimensions=dimensions,
				      lower_bounds=l_bounds,
				      upper_bounds=u_bounds,
				      minimise=True)