#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>

#ifdef _WIN32
    #include <WINDOWS.H>
#elif _WIN64
	#include <WINDOWS.H>
#endif

void cec17_test_func(double *, double *,int,int,int);

double *OShift,*M,*y,*z,*x_bound;
int ini_flag=0,n_flag,func_flag,*SS;

using namespace std;

int main(int argc, char* argv[])
{

	int func_num = (int) strtod(argv[1], NULL);
	int num_dim = (int) strtod(argv[2], NULL);
	int num_part = (int) strtod(argv[3], NULL);
	string file = argv[4];

	int dimX = num_part * num_dim;

	double *f,*x;	
	//x = (double *) malloc( (dimX) * sizeof(double));
	f = (double *) malloc( num_part * sizeof(double));

	// for (int i = 0; i < dimX; i++)
	// {
	// 	x[i] = strtod(argv[i+4], NULL);
	// 	//cout << x[i] << "\n";
	// }

	//vector<vector<double> > matrix;
	
	string line;
	ifstream infile(file.c_str());
	vector<double> v1;
	while (getline(infile, line))
	{
		//v1.clear();
		istringstream buf(line);
		for(string token; getline(buf, token, '\t');)
		{
			double x = strtod(token.c_str(), NULL);
			v1.push_back(x);
		}
		//matrix.push_back(v1);
    }

    // for(int i=0; i < v1.size(); i++)
    // 	cout << i << "\t" << v1[i] << "\n";

    x = &v1[0];

	cec17_test_func(x, f, num_dim, num_part, func_num);
	
	for (int i = 0; i < num_part; i++)
	{	
		if( i== (num_part-1))
			printf("%f", f[i]);
		else
			printf("%f\t", f[i]);
	}

/*	v1.clear();
	free(x);
	free(f);*/
	
	
	return 0;
}


