import os, sys, shutil	
import numpy as np

sys.path.insert(0, "RandomSearch")

from RandomSearch import RandomSearch
from subprocess import check_output, check_call


class RandomSearch_cec2017(RandomSearch):

	def __init__(self, n_particles=100, max_iterations=100, path=".", repetition=0, ID=None, seed=None, verbose=True):
		super().__init__(n_particles=n_particles, max_iterations=max_iterations, path=path, repetition=repetition, ID=ID, seed=seed, verbose=verbose)


	def update_fitness(self):

		self.__function      = self.get_function()
		self.__communication = 'bin' + os.sep + 'RandomSearch_positions_f%s'%self.__function

		from sys import platform as _platform

		if _platform == "linux" or _platform == "linux2" or _platform == "darwin" :
			BRIDGEexec = "./bin/bridge"
		else:
			BRIDGEexec = "./bin/bridge.exe"

		toWrite = []
		for p in self.get_particles():
			toWrite.append(p.get_X())

		np.savetxt(self.__communication, toWrite, delimiter='\t', fmt='%.8f')


		args1 = [BRIDGEexec, self.__function, str(len(p.get_X())), str(len(self.get_particles())), self.__communication]
		retBridge = check_output(args1).decode("utf-8")

		ind = 0
		for idx,p in enumerate(self.get_particles()):
			fitness = float(retBridge.split("\t")[idx])
			p.set_fitness(fitness)

		
if __name__ == '__main__':

	repetitions = 30
	dimensions  = 30

	n_particles = 50
	max_it      = (10000 * dimensions)//n_particles

	functions  = ["%s"%(f+1) for f in range(30)]

	for function in functions:
		print("*"*100)
		print(" * Optimizing function: %s"%function)

		path = "ResultsCEC2017"+os.sep+"RandomSearch"+os.sep+"f%s"%function+"_"+str(dimensions)

		l_bounds = [-100]
		u_bounds = [100]
		
		for rep in range(repetitions):
			print(" \t Running optimization %3d/%d"%(rep+1,repetitions))
			rs = RandomSearch_cec2017(n_particles=n_particles,
									  max_iterations=max_it,
									  path=path,
									  repetition=rep,
									  verbose=False,
									  seed=rep)

			rs.solve(function=function,
				     dimensions=dimensions,
				     lower_bounds=l_bounds,
				     upper_bounds=u_bounds,
				     minimise=True)	