import sys, os, copy, math
import numpy as np
from benchmarks import *


class Particle(object):

	def __init__(self):
		self.__X = []
		self.__fitness = sys.float_info.max

	
	def __repr__(self):
		return str(list(self.__X))

	
	def __str__(self):
		return "\t".join(map(str, list(self.__X)))

	
	def get_X(self):
		return self.__X

	
	def set_X(self, X):
		self.__X = X

	
	def get_fitness(self):
		return self.__fitness

	
	def set_fitness(self, fitness):
		self.__fitness = fitness


class RandomSearch(object):

	def __repr__(self):
		return str(self.__ID)

	
	def __init__(self, n_particles=100, max_iterations=100, path=".", repetition=0, ID=None, seed=None, verbose=True):

		if ID is not None:
			self.__ID = ID

		if seed is not None:
			np.random.seed(seed)

		self.__B = None
		self.__n_particles = n_particles
		self.__max_iterations = max_iterations
		self.__iteration = 1
		self.__dimensions = 10
		self.__fitness = None
		self.__particles = []
		self.__repetition = repetition

		self.__verbose = verbose
		self.__path = path
		self.__minimise = True
		self.__lower_bounds = None
		self.__upper_bounds = None
		self.__function = None

		if not os.path.exists(self.__path):
			os.makedirs(self.__path)

		file = self.__path + os.sep + "random_rep" + str(self.__repetition) + "_fitness"
		if os.path.isfile(file):
			os.remove(file)
		
		file = self.__path + os.sep + "random_rep" + str(self.__repetition) + "_solution"
		if os.path.isfile(file):
			os.remove(file)

	
	def __initialise(self):

		return np.random.uniform(0, 1, self.__dimensions) * (self.__upper_bounds - self.__lower_bounds) + self.__lower_bounds


	def __create_particles(self):

		for i in range(self.__n_particles):
			p = Particle()
			p.set_X(self.__initialise())
			self.__particles.append(p)

		self.update_fitness()

		if self.__minimise:
			self.__particles = sorted(self.__particles, key=lambda x: x.get_fitness(), reverse=False)
		else:
			self.__particles = sorted(self.__particles, key=lambda x: x.get_fitness(), reverse=True)

		self.__B = copy.deepcopy(self.__particles[0])

		if self.__verbose:
			print(" * %d particles have been created\n" % self.__n_particles)
			print(" * Iteration %5d: best fitness %5.2f" % (self.__iteration, self.__B.get_fitness()))

		self.__write_files()
	

	def __termination_criterion(self):

		if self.__iteration > self.__max_iterations - 1:
			if self.__verbose:
				print("\nThe maximum number of allowed iterations have been reached")
			return True
		else:
			return False


	def __iterate(self):
		
		for p in self.__particles:
			X = self.__initialise()
			p.set_X(X)

		self.update_fitness()

		for p in self.__particles:

			if self.__minimise:
				if p.get_fitness() < self.__B.get_fitness():
					self.__B = copy.deepcopy(p)
			else:
				if p.get_fitness() > self.__B.get_fitness():
					self.__B = copy.deepcopy(p)

		self.__iteration += 1

		if self.__verbose:
			print(" * Iteration %5d: best fitness %5.2f" % (self.__iteration, self.__B.get_fitness()))

		self.__write_files()

	
	def __write_files(self):

		with open(self.__path + os.sep + "random_rep" + str(self.__repetition) + "_fitness", "a") as fo:
			fo.write("%.8e" % self.__B.get_fitness() + "\n")

		with open(self.__path + os.sep + "random_rep" + str(self.__repetition) + "_solution", "a") as fo1:
			X = copy.deepcopy(self.__B.get_X())
			for i in range(len(X)):
				fo1.write(str(X[i]) + "\t")
			fo1.write("\n")


	def get_function(self):
		return self.__function


	def get_particles(self):
		return self.__particles


	def update_fitness(self):
		try:
			for s in self.__particles:
				fitness = self.__function(s.get_X())
				s.set_fitness(fitness)
		except:
			print("* Error! The function", self.__function, "is not defined. Please, provide a valid function.")
			exit(-1)

	
	def solve(self, function=None, dimensions=10, lower_bounds=[-5], upper_bounds=[5], minimise=True):

		self.__dimensions = dimensions

		if len(lower_bounds) < dimensions:
			lower_bounds = [lower_bounds[0]] * dimensions

		if len(upper_bounds) < dimensions:
			upper_bounds = [upper_bounds[0]] * dimensions

		if not isinstance(lower_bounds, np.ndarray):
			self.__lower_bounds = np.array(lower_bounds)
		if not isinstance(upper_bounds, np.ndarray):
			self.__upper_bounds = np.array(upper_bounds)

		self.__function = function
		self.__minimise = minimise

		if self.__verbose:
			print("Starting Random Search ...\n")

		self.__create_particles()

		while not self.__termination_criterion():
			self.__iterate()

		if self.__verbose:
			print("Process terminated: best fitness %5.2f"%self.__B.get_fitness())
			print("Best positions:\n", self.__B.get_X())

			dimensions

		return self.__B.get_X(), self.__B.get_fitness()