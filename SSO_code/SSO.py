import sys, os, copy, math
import numpy as np
from benchmarks import *


class Salp(object):

	def __init__(self):
		self.__X = []
		self.__fitness = sys.float_info.max

	
	def __repr__(self):
		return str(list(self.__X))

	
	def __str__(self):
		return "\t".join(map(str, list(self.__X)))

	
	def get_X(self):
		return self.__X

	
	def set_X(self, X):
		self.__X = X

	
	def get_fitness(self):
		return self.__fitness

	
	def set_fitness(self, fitness):
		self.__fitness = fitness


class SSO(object):

	def __repr__(self):
		return str(self.__ID)

	
	def __init__(self, n_salps=100, max_iterations=100, path=".", repetition=0, ID=None, seed=None, verbose=True):

		if ID is not None:
			self.__ID = ID

		if seed is not None:
			np.random.seed(seed)

		self.__F = None
		self.__c1 = None
		self.__n_salps = n_salps
		self.__max_iterations = max_iterations
		self.__iteration = 1
		self.__dimensions = 10
		self.__fitness = None
		self.__salps = []
		self.__repetition = repetition

		self.__verbose = verbose
		self.__path = path
		self.__minimise = True
		self.__lower_bounds = None
		self.__upper_bounds = None
		self.__function = None

		if not os.path.exists(self.__path):
			os.makedirs(self.__path)

		file = self.__path + os.sep + "sso_code_rep" + str(self.__repetition) + "_fitness"
		if os.path.isfile(file):
			os.remove(file)
		
		file = self.__path + os.sep + "sso_code_rep" + str(self.__repetition) + "_solution"
		if os.path.isfile(file):
			os.remove(file)


	def __initialise(self):

		return np.random.uniform(0, 1, self.__dimensions) * (self.__upper_bounds - self.__lower_bounds) + self.__lower_bounds


	def __create_salps(self):

		for i in range(self.__n_salps):
			s = Salp()
			s.set_X(self.__initialise())
			self.__salps.append(s)

		self.update_fitness()

		if self.__minimise:
			self.__salps = sorted(self.__salps, key=lambda x: x.get_fitness(), reverse=False)
		else:
			self.__salps = sorted(self.__salps, key=lambda x: x.get_fitness(), reverse=True)

		self.__F = copy.deepcopy(self.__salps[0])

		if self.__verbose:
			print(" * %d salps have been created\n" % self.__n_salps)
			print(" * Iteration %5d: best fitness %5.2f" % (self.__iteration, self.__F.get_fitness()))

		self.__write_files()

	
	def __termination_criterion(self):

		if self.__iteration > self.__max_iterations - 1:
			if self.__verbose:
				print("\nThe maximum number of allowed iterations have been reached")
			return True
		else:
			return False

	
	# Eq. (3.2) in the paper
	def __updateC1(self):
		
		self.__c1 = 2 * math.exp(-((4 * self.__iteration / self.__max_iterations) ** 2))

	
	def __update_leader(self, ind):

		c2 = np.random.uniform(0, 1, self.__dimensions)
		c3 = np.random.uniform(0, 1, self.__dimensions)

		F = copy.deepcopy(self.__F.get_X())
		X = np.zeros(self.__dimensions)

		for i in range(self.__dimensions):

			if c3[i] < 0.5:
				X[i] = F[i] + self.__c1 * ((self.__upper_bounds[i] - self.__lower_bounds[i]) * c2[i] + self.__lower_bounds[i])
			else:
				X[i] = F[i] - self.__c1 * ((self.__upper_bounds[i] - self.__lower_bounds[i]) * c2[i] + self.__lower_bounds[i])

		self.__salps[ind].set_X(X)


	def __update_salps(self):

		for i in range(self.__n_salps):
			
			# Eq. (3.1) in the paper
			if i < self.__n_salps / 2:
				self.__update_leader(i)
			
			# Eq. (3.4) in the paper
			else:
				X1 = copy.deepcopy(self.__salps[i].get_X())
				X2 = copy.deepcopy(self.__salps[i-1].get_X())
				X  = 0.5 * (X1 + X2)
				self.__salps[i].set_X(X)


	def __iterate(self):
		self.__updateC1()
		self.__update_salps()

		for s in self.__salps:
			X = copy.deepcopy(s.get_X())
			for i in range(self.__dimensions):
				X[i] = np.clip(X[i], self.__lower_bounds[i], self.__upper_bounds[i])

			s.set_X(X)

		self.update_fitness()

		for s in self.__salps:

			if self.__minimise:
				if s.get_fitness() < self.__F.get_fitness():
					self.__F = copy.deepcopy(s)
			else:
				if s.get_fitness() > self.__F.get_fitness():
					self.__F = copy.deepcopy(s)

		self.__iteration += 1

		if self.__verbose:
			print(" * Iteration %5d: best fitness %5.2f" % (self.__iteration, self.__F.get_fitness()))

		self.__write_files()

	
	def __write_files(self):

		with open(self.__path + os.sep + "sso_code_rep" + str(self.__repetition) + "_fitness", "a") as fo:
			fo.write("%.8e" % self.__F.get_fitness() + "\n")

		with open(self.__path + os.sep + "sso_code_rep" + str(self.__repetition) + "_solution", "a") as fo1:
			X = copy.deepcopy(self.__F.get_X())
			for i in range(len(X)):
				fo1.write(str(X[i]) + "\t")
			fo1.write("\n")


	def get_function(self):
		return self.__function


	def get_salps(self):
		return self.__salps


	def update_fitness(self):
		try:
			for s in self.__salps:
				fitness = self.__function(s.get_X())
				s.set_fitness(fitness)
		except:
			print("* Error! The function", self.__function, "is not defined. Please, provide a valid function.")
			exit(-1)
	

	def solve(self, function=None, dimensions=10, lower_bounds=[-5], upper_bounds=[5], minimise=True):

		self.__dimensions = dimensions

		if len(lower_bounds) < dimensions:
			lower_bounds = [lower_bounds[0]] * dimensions

		if len(upper_bounds) < dimensions:
			upper_bounds = [upper_bounds[0]] * dimensions

		if not isinstance(lower_bounds, np.ndarray):
			self.__lower_bounds = np.array(lower_bounds)
		if not isinstance(upper_bounds, np.ndarray):
			self.__upper_bounds = np.array(upper_bounds)

		self.__function = function
		self.__minimise = minimise

		if self.__verbose:
			print("Starting SSO code...\n")

		self.__create_salps()

		while not self.__termination_criterion():
			self.__iterate()

		if self.__verbose:
			print("Process terminated: best fitness %5.2f"%self.__F.get_fitness())
			print("Best positions:\n", self.__F.get_X())

		return self.__F.get_X(), self.__F.get_fitness()