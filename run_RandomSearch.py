import os, sys, shutil	
import numpy as np

sys.path.insert(0, "RandomSearch")

from RandomSearch import RandomSearch
from Benchmarks import Benchmarks


class RandomSearch_ben(RandomSearch):

	def __init__(self, n_particles=100, max_iterations=100, path=".", repetition=0, ID=None, seed=None, verbose=True):
		super().__init__(n_particles=n_particles, max_iterations=max_iterations, path=path, repetition=repetition, ID=ID, seed=seed, verbose=verbose)


	def update_fitness(self):

		benchmarks  = Benchmarks()

		try:
			for p in self.get_particles():
				fitness = eval("benchmarks." + self.get_function() + "(p.get_X())")
				p.set_fitness(fitness)
		except:
			print("* Error! The function", self.get_function(), "is not defined. Please, provide a valid function.")
			exit(-1)
		
		
if __name__ == '__main__':

	benchmarks  = Benchmarks()
	boundaries  = benchmarks.Boundaries()

	repetitions = 30
	dimensions  = 2

	n_particles = 50
	max_it  = 100

	functions  = ["Ackley_shifted",
				  "Alpine_shifted",
				  "Rosenbrock_shifted",
				  "Sphere_shifted"]

	for function in functions:
		print("*"*100)
		print(" * Optimizing function: %s"%function)

		path = "Results"+os.sep+"RandomSearch"+os.sep+function+"_"+str(dimensions)

		bounds   = boundaries[function]
		l_bounds = [bounds[0]]
		u_bounds = [bounds[1]]
		
		for rep in range(repetitions):
			print(" \t Running optimization %3d/%d"%(rep+1,repetitions))
			rs = RandomSearch_ben(n_particles=n_particles,
								  max_iterations=max_it,
								  path=path,
								  repetition=rep,
								  verbose=False,
								  seed=rep)

			rs.solve(function=function,
				     dimensions=dimensions,
				     lower_bounds=l_bounds,
				     upper_bounds=u_bounds,
				     minimise=True)