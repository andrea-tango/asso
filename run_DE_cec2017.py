from pymoo.algorithms.so_de import DE
from pymoo.model.problem import Problem
from pymoo.model.callback import Callback
from pymoo.optimize import minimize

import os, sys, shutil
import numpy as np
from subprocess import check_output, check_call

class CEC2017(Problem):
	
	def __init__(self, n_dimensions=10, function='1', xl = -100, xu = 100):
		
		self.__n_dimensions  = n_dimensions
		self.__function      = function
		self.__communication = 'bin' + os.sep + 'DE_positions_f%s'%self.__function

		super().__init__(n_var=n_dimensions,
						 n_obj=1,
						 n_constr=0,
						 xl=xl,
						 xu=xu)
		
	def _evaluate(self, X, out, *args, **kwargs):
		
		from sys import platform as _platform
		if _platform == "linux" or _platform == "linux2" or _platform == "darwin" :
			BRIDGEexec = "./bin/bridge"
		else:
			BRIDGEexec = "./bin/bridge.exe"
		
		toWrite = []
		for x in X:
			toWrite.append(x)
		
		np.savetxt(self.__communication, toWrite, delimiter='\t', fmt='%.8f')
		
		n_individuals = len(X)
				
		args1 = [BRIDGEexec, self.__function, str(self.__n_dimensions), str(n_individuals), self.__communication]
		retBridge = check_output(args1).decode("utf-8")

		f1 = []
		for idx in range(n_individuals):
			fitness = float(retBridge.split("\t")[idx])
			f1.append(fitness)

		f1 = np.array(f1)
		
		out["F"] = np.column_stack([f1])
	

if __name__ == '__main__':

	repetitions = 30
	dimensions  = 30

	n_individuals = 50
	max_it        = (10000 * dimensions)//n_individuals

	functions  = ["%s"%(f+1) for f in range(30)]
	
	for function in functions:
		print("*"*100)
		print(" * Optimizing function: %s"%function)

		path = "ResultsCEC2017"+os.sep+"DE"+os.sep+"f%s"%function+"_"+str(dimensions)
		
		if not os.path.exists(path):
			os.makedirs(path)

		l_bounds = -100
		u_bounds = 100
		
		for rep in range(repetitions):
			print(" \t Running optimization %3d/%d"%(rep+1,repetitions))
		
			problem   = CEC2017(n_dimensions=dimensions,
								function=function,
								xl = l_bounds,
								xu = u_bounds)

			algorithm = DE(pop_size=n_individuals)

			res = minimize(problem,
						   algorithm,
						   ('n_iter', max_it),
						   seed=rep,
						   verbose=False,
						   save_history=True)

			indeces   = [e.pop.get("F").argmin() for e in res.history]
			fitnesses = [e.pop.get("F")[indeces[idx]] for idx,e in enumerate(res.history)]
			solutions = [e.pop.get("X")[indeces[idx]] for idx,e in enumerate(res.history)]

			if len(fitnesses) < max_it:
				fitnesses = fitnesses + [fitnesses[-1]]*(max_it - len(fitnesses))
				solutions = solutions + [solutions[-1]]*(max_it - len(solutions))
			
			np.savetxt(path + os.sep + "de_rep" + str(rep) + "_fitness",  fitnesses, delimiter='\n', fmt='%.8f')
			np.savetxt(path + os.sep + "de_rep" + str(rep) + "_solution", solutions, delimiter='\t', fmt='%.8f')