# Amended Salp Swarm Optimizer (ASSO)

_Amended Salp Swarm Optimizer (ASSO)_ is a novel and mathematically more robust formulation of the Salp Swarm Optimizer (SSO) algorithm [1].
SSO [2] is a swarm intelligence global optimization method inspired by salp colonies. Salps are barrel-shaped animals living in swarms, organized in long chains, which actively look for phytoplankton. 
The authors took inspiration from this peculiar spatial arrangement to introduce a novel optimization algorithm, where the individuals of the population are constrained to follow the leader salp while it is performing the actual exploration for food sources. 
In this metaphor, the leader explores the search space for optimal regions, with respect to the provided fitness function, while the followers exploit the area surrounding the leader.

In order to use ASSO, the user has to implement a custom fitness function and specify the boundaries of the search space.
The authors can specify a different lower bound and upper bound for each dimension; otherwise, the same lower bound and upper bound is assumed.
The user can optionally specify other parameter settings (e.g., the number of salps and the number of iterations). When the stopping criterion is met, ASSO returns the best solution found so far as well as its fitness value.

We also provide the code of SSO as described by the pseudo-code and explanations of the original paper [2], a robust Python implementation of the code written by the authors, and the code of a simple Random Search. 
These implementations have been used to generate the results shown in [1].

ASSO and the other algorithms are completely written in Python and only rely on the NumPy package. 


## Example

ASSO can be easily used as follows:

	import sys
    from ASSO import ASSO	
	
	def fitness( salp ):
		return sum(map(lambda x: x**2 - x, salp))
		
	if __name__ == '__main__':
		asso = ASSO(n_salps=50, max_iterations=100, path="Output_example", repetition=0, seed=42)
		asso.solve(function=fitness, dimensions=10, lower_bounds=[-10], upper_bounds=[10], minimise=True)


## Further information

ASSO has been created by Mauro Castelli (Universidade Nova de Lisboa), Luca Manzoni (University of  Trieste), Luca Mariot (Delft University of Technology), Marco S. Nobile (Eindhoven University of Technology), and Andrea Tangherloni (University of Bergamo). The source code has been written and maintained by Andrea Tangherloni.

If you need any information about ASSO or the other algorithms in this repository please write to: andrea.tangherloni@unibg.it


[1] Castelli M., Manzoni L., Mariot L., Nobile M.S., and Tangherloni A.: _Salp Swarm Optimization: a Critical Review_, <a href="https://arxiv.org/abs/2106.01900">arXiv</a>, arXiv:2106.01900, 2021.

[2] Mirjalili S., Gandomi A.H., Mirjalili S.Z., Saremi S., Faris H., and Mirjalili S.M. _Salp Swarm Algorithm: A bio-inspired optimizer for engineering design problems_. Advances in Engineering Software, 114:163-191, 2017 (doi:10.1016/j.advengsoft.2017.07.002).
